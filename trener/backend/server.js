const http = require('http')


console.log("witaj w node");


const server = http.createServer(function (req, res) {
  console.log('Zapytanie');
  console.log('URL', req.url);
  console.log('Headers', req.rawHeaders);
  // req.headers.range 1500-22000

  res.setHeader('Content-Type', 'text/html; charset=UTF-8')
  res.write('<html><header><meta charset="utf-8"></header><body>')
  res.write('<h1>Witaj na serwerze</h1>')
  res.write('<p style="color:hotpink">Miło Cię hościć</p>')
  count = 0
  setInterval(function () {
    res.write('<p>' + (new Date()).toLocaleTimeString() + '</p>')
    if (count++ > 10)
      res.end()
  }, 1000)
})

server.listen(3000, 'localhost', function () {
  console.log('Slucham na http://localhost:3000/');
})